﻿/*
 * Copyright 2017 James Davidson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;

public partial class MainPage : Page {

    // Lifecycle Events

    protected void Page_PreInit() {
        if (!IsPostBack) {
            Session["CoverageValue"] = 0;
            Session["CustomCoverage"] = 0;
            Session["PaintBrandChosen"] = 0;
            Session["DoorHeight"] = 0;
            Session["DoorWidth"] = 0;
            Session["PaintNeeded"] = 0;
            Session["SingleDoors"] = 0;
            Session["DoubleDoors"] = 0;
        }
    }

    protected void Page_Init() {
        RestrictTextboxInput();
        // Set the text of BQ to B&Q.
        ddlPaintBrand.SelectedIndex = 1;
        ddlPaintBrand.SelectedItem.Text = "B&Q";
        ddlPaintBrand.SelectedIndex = -1;
    }

    // Event Handlers

    protected void UpdateResult(object sender, EventArgs e) {
            DisplayResult();
    }

    protected void ddlNumberOfWalls_SelectedIndexChanged(object sender, EventArgs e) {
        int numberOfWalls = int.Parse(ddlNumberOfWalls.SelectedValue);
        // Wall 1 Width is always visible, so do not try to render it.
        const int defaultWallWidthTextboxCount = 1;
        int panelCount = numberOfWalls - defaultWallWidthTextboxCount;
        ShowPanel(panelCount, "pnlWall");
        // Textboxes are available for up to 8 walls, where 1 is visible by default.
        // 'Optional' textboxes are placed into panels are easy hiding/unhiding.
        const int maxPanels = 8 - defaultWallWidthTextboxCount;
        HidePanel(maxPanels, panelCount, "pnlWall", "txtWallWidth");
    }

    protected void cbCeiling_CheckedChanged(object sender, EventArgs e) {
        // Used to either display the ceiling panel, or to hide it - based on the Checked property of checkbox1.
        UpdateCeilingPanel(cbCeiling, pnlCeiling, txtCeilingHeight, txtCeilingWidth);
    }

    protected void ddlNumberOfWindows_SelectedIndexChanged(object sender, EventArgs e) {
        int numberOfWindows = int.Parse(ddlNumberOfWindows.SelectedValue);
        int panelCount = numberOfWindows;
        ShowPanel(panelCount, "pnlWindow");
        // Panels are available for up to 4 windows, height and width 
        // textboxes are placed into each panel.
        const int maxPanels = 4;
        HidePanel(maxPanels, panelCount, "pnlWindow", "txtWindowHeight", "txtWindowWidth");
    }

    protected void ddlPaintBrand_SelectedIndexChanged(object sender, EventArgs e) {
        string brandName = ddlPaintBrand.SelectedItem.Text;
        // Used to determine whether the value selected is equal to 0.
        int brandNumber = int.Parse(ddlPaintBrand.SelectedValue);
        DisplayDropdownlist(brandName, brandNumber);
    }

    protected void btnClear_Click(object sender, EventArgs e) {
        // Empty the custom coverage textbox and re-calculate the result.
        EmptyTextbox(txtCustomCoverage);
        DisplayResult();
    }

    // TODO: Configure all dropdownlists to fire the same SelectedIndexChanged
    // event handler, grabbing the control name and brand name programatically.

    protected void ddlDulux_SelectedIndexChanged(object sender, EventArgs e) {
        SendCoverageValue(ddlDulux, "Dulux");
    }

    protected void ddlWickes_SelectedIndexChanged(object sender, EventArgs e) {
        SendCoverageValue(ddlWickes, "Wickes");
    }

    protected void ddlCrown_SelectedIndexChanged(object sender, EventArgs e) {
        SendCoverageValue(ddlCrown, "Crown");
    }

    protected void ddlJohnstones_SelectedIndexChanged(object sender, EventArgs e) {
        SendCoverageValue(ddlJohnstones, "Johnstones");
    }

    protected void ddlBQ_SelectedIndexChanged(object sender, EventArgs e) {
        SendCoverageValue(ddlBQ, "BQ");
    }

    protected void ddlLeyland_SelectedIndexChanged(object sender, EventArgs e) {
        SendCoverageValue(ddlLeyland, "Leyland");
    }

    protected void ddlValspar_SelectedIndexChanged(object sender, EventArgs e) {
        SendCoverageValue(ddlValspar, "Valspar");
    }

    protected void txtCustomCoverage_TextChanged(object sender, EventArgs e) {
        // Save the custom coverage value, then display the new result using that value.
        double coverageValue;
        double.TryParse(txtCustomCoverage.Text, out coverageValue);
        Session["CustomCoverage"] = coverageValue;

        DisplayResult();
    }

    // Methods

    /// <summary>
    /// Used to update the CeilingPanel specifically. 
    /// <para>Can be used on any single panel whose visiblity is controlled by a checkbox.</para>
    /// </summary>
    /// <param name="checkbox">The checkbox used to hide and unhide the panel.</param>
    /// <param name="panel">The panel which contains the textbox controls.</param>
    /// <param name="textbox1">The ID of the first textbox in the panel.</param>
    /// <param name="textbox2">The ID of the second textbox in the panel.</param>

    private void UpdateCeilingPanel(CheckBox checkbox, Panel panel, TextBox textbox1, TextBox textbox2) {
        if (checkbox.Checked)
            panel.Visible = true;
        else {
            double oldArea = CalculateRoomArea();
            panel.Visible = false;
            
            // Empty the textboxes.
            EmptyTextbox(textbox1);
            EmptyTextbox(textbox2);
            
            // And see if this produces a different result.
            double newArea = CalculateRoomArea();
            if (newArea != oldArea)
                DisplayResult();
        }
    }

    /// <summary>
    /// Returns the ID of the selected child control which exists under a master dropdownlist.
    /// <para>Only one request boolean can be set to true. Otherwise the method will return null.</para>
    /// </summary>
    /// <param name="masterDropdownlist">The master dropdownlist control.</param>
    /// <param name="requestDropdownlist">The child dropdownlist to get.</param>
    /// <param name="requestPanel">The child panel to get.</param>
    /// <returns>Returns the appropriate control ID.</returns>

    private string ControlInUse(DropDownList masterDropdownlist, bool requestDropdownlist, bool requestPanel = false) {
        int id = int.Parse(masterDropdownlist.SelectedValue);
        if (id == 0)
            return null;

        string brandName = GetPaintBrand(id);
        string result = "";

        if (requestDropdownlist == true)
            result = "ddl" + brandName;

        return result;
    }

    /// <summary>
    /// Renders the results of CalculateRoomArea() and CalculatePaintNeeded() onto the page.
    /// <para>Sends an error message and stops execution if CalculateRoomArea() returns a negative value.</para>
    /// </summary>

    private void DisplayResult() {
        double areaOfRoom = CalculateRoomArea();
        // When the page loads, there are no values. Skip showing the result here.
        // TODO: Clean up this code.
        if (areaOfRoom == 0)
            return;

        bool isAreaNegative = IsAreaNegative(areaOfRoom);
        if (isAreaNegative == true) {
            SendNegativeAreaError();
            return;
        }

        // Assume that productId = 1 (blank), unless a value is parsed from the master dropdownlist.
        int productId = 1;
        int coatsOfPaint = int.Parse(ddlCoatsOfPaint.SelectedValue);


        string ddlId = ControlInUse(ddlPaintBrand, true, false);
        if (ddlId != null) {
            DropDownList ddlInUse = (DropDownList)FindControl(ddlId);
            int.TryParse(ddlInUse.SelectedValue, out productId);
        }

        string caseSwitch = GetCaseSwitch(productId);
        if (caseSwitch != "select")
            DrawResult(caseSwitch, coatsOfPaint, areaOfRoom);
        else {
            string table = ddlId.Substring(3);
            DrawResult(caseSwitch, coatsOfPaint, areaOfRoom, table, productId);
        }
        // Make the results visible.
        pnlResult.Visible = true;
        pnlCoverage.Visible = true;
    }

    private void DrawPrice(int productId, string table, double totalPaintNeeded) {
            string price = QueryDatabase("CostPerLitre", table, productId);
            double costPerLitre = double.Parse(price);
            double cost = costPerLitre * totalPaintNeeded;
            string message = "This paint costs " + cost + " per litre.";
            Response.Write(message);
        }

    public string GetCaseSwitch(int productId) {
        if (txtCustomCoverage.Text == "" && productId == 1)
            return "default";
        else if (txtCustomCoverage.Text != "")
            return "custom";
        else
            return "select";
    }

    public void DrawResult(string caseSwitch, int coatsOfPaint, double areaOfRoom, string tableName = null, int productId = 1) {
        double totalPaintNeeded;

        switch (caseSwitch) {
            case "default":
                int defaultCoverage = 12;
                totalPaintNeeded = CalculatePaintNeeded(areaOfRoom, coatsOfPaint, defaultCoverage);
                lblCoverageValue.Text = "Using the default coverage value of " + defaultCoverage + "m² per litre.";
                hlDefaultCoverage.Visible = true;
                lblPaintNeeded.Text = "You need: " + totalPaintNeeded.ToString("0.00") + " litres of paint.";
                lblRoomArea.Text = "The total area of the room is: " + areaOfRoom.ToString("0.00") + "m².";
                break;
            case "custom":
                double customCoverage = Convert.ToDouble(Session["CustomCoverage"]);
                totalPaintNeeded = CalculatePaintNeeded(areaOfRoom, coatsOfPaint, customCoverage);
                lblCoverageValue.Text = "Using the custom coverage value of " + customCoverage + "m² per litre.";
                hlDefaultCoverage.Visible = false;
                lblPaintNeeded.Text = "You need: " + totalPaintNeeded.ToString("0.00") + " litres of paint.";
                lblRoomArea.Text = "The total area of the room is: " + areaOfRoom.ToString("0.00") + "m².";
                break;
            case "select":
                double coverageValue = Convert.ToDouble(Session["CoverageValue"]);
                totalPaintNeeded = CalculatePaintNeeded(areaOfRoom, coatsOfPaint, coverageValue);
                string paintName = QueryDatabase("Paint", tableName, productId);
                string paintURL = QueryDatabase("URL", tableName, productId);
                string htmlString = "<a target='_blank' href='" + paintURL + "'" + ">" + paintName + "</a>";
                lblCoverageValue.Text = "The coverage value for " + Server.HtmlDecode(htmlString) + " is " + coverageValue + "m² per litre.";
                hlDefaultCoverage.Visible = false;
                lblPaintNeeded.Text = "You need: " + totalPaintNeeded.ToString("0.00") + " litres of paint.";
                lblRoomArea.Text = "The total area of the room is: " + areaOfRoom.ToString("0.00") + "m².";
                // BUG: Calling Response.Write within a case throws an exception.
                break;
        }
    }

    private void SendNegativeAreaError2(double wallArea, double ceilingArea, double windowArea, double doorArea) {
        // Note: The first if condition to be true isn't hit. The statement to execute relies on whether windowArea or doorArea is bigger.
        string message;

        if (wallArea + ceilingArea < windowArea) {
            message = "The area of the windows exceeds the area of the room.";
            SendMessage(message, lblPaintNeeded, lblRoomArea);
        } else if (wallArea + ceilingArea < doorArea) {
            message = "The area of the doors exceeds the area of the room.";
            SendMessage(message, lblPaintNeeded, lblRoomArea);
        } else if (wallArea + ceilingArea < windowArea + doorArea) {
            // This message will be skipped if either windowArea or doorArea are larger than wallArea + ceilingArea.
            // TODO: Fix this logic.
            message = "The combined area of the doors and windows exceed the area of the room.";
            SendMessage(message, lblPaintNeeded, lblRoomArea);
        }
    }

    private void SendNegativeAreaError() {
        string caseSwitch = CauseOfNegativity(CalculateWallArea(), CalculateCeilingArea(), CalculateWindowArea(), CalculateDoorArea());
        string message;

        switch (caseSwitch) {
            case "unknown":
                message = "Error: The area of the room is negative.";
                SendMessage(message, lblPaintNeeded, lblRoomArea);
                break;
            case "combined":
                message = "The combined area of the doors and windows exceed the area of the room.";
                SendMessage(message, lblPaintNeeded, lblRoomArea);
                break;
            case "doors":
                message = "The area of the doors exceeds the area of the room.";
                SendMessage(message, lblPaintNeeded, lblRoomArea);
                break;
            case "windows":
                message = "The area of the windows exceeds the area of the room.";
                SendMessage(message, lblPaintNeeded, lblRoomArea);
                break;
        }
    }

    private string CauseOfNegativity(double wallArea, double ceilingArea, double windowArea, double doorArea) {
        // Combined area of windowArea & doorArea is larger than wallArea & ceilingArea.
        if (windowArea > 0 && doorArea > 0) {
            if (wallArea + ceilingArea < windowArea + doorArea)
                return "combined";
        }
        if (wallArea + ceilingArea < windowArea)
            return "windows";
        else if (wallArea + ceilingArea < doorArea)
            return "doors";
        else
            return "unknown";
        }

    private bool IsAreaNegative(double areaOfRoom) {
        if (areaOfRoom < 0)
            return true;
        else
            return false;
    }

    /// <summary>
    /// Gets the correct coverage value for the paint selected, then displays the new result.
    /// </summary>
    /// <param name="paintDdl">The dropdownlist in use. The ID should correlate to the paint brand selected by the master control.</param>
    /// <param name="paintBrand">The name of the paint brand.</param>

    protected void SendCoverageValue(DropDownList paintDdl, string paintBrand) {
        double coverageValue;
        if (paintDdl.SelectedIndex != 0) {
            int productID = int.Parse(paintDdl.SelectedValue);
            coverageValue = GetCoverageValue(paintBrand, productID, txtCustomCoverage);
            Session["CoverageValue"] = coverageValue;
        }
        DisplayResult();
    }

    /// <summary>
    /// Restricts all textboxes to accept allowed inputs: (0-9) or (.)
    /// <para>See the Javascript function: isDecimal()</para>
    /// </summary>

    private void RestrictTextboxInput() {
        txtSingleDoors.Attributes.Add("onkeypress", "return isNumberKey(event)");
        txtDoubleDoors.Attributes.Add("onkeypress", "return isNumberKey(event)");

        txtCustomCoverage.Attributes.Add("onkeypress", "return isDecimal(event)");

        txtWallHeight.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWallWidth1.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWallWidth2.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWallWidth3.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWallWidth4.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWallWidth5.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWallWidth6.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWallWidth7.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWallWidth8.Attributes.Add("onkeypress", "return isDecimal(event)");

        txtWindowHeight1.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWindowWidth1.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWindowHeight2.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWindowWidth2.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWindowHeight3.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWindowWidth3.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWindowHeight4.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtWindowWidth4.Attributes.Add("onkeypress", "return isDecimal(event)");

        txtCeilingHeight.Attributes.Add("onkeypress", "return isDecimal(event)");
        txtCeilingWidth.Attributes.Add("onkeypress", "return isDecimal(event)");
    }

    /// <summary>
    /// Resets the Text property of a textbox to be equal to "".
    /// <para>This prevents an inaccurate result being rendered by DisplayResult().</para>
    /// </summary>
    /// <param name="textbox">The textbox control you wish to reset.</param>

    private void EmptyTextbox(TextBox textbox) {
        textbox.Text = "";
    }

    /// <summary>
    /// Sends a message to the user: the message can be rendered onto the page, or in a window alert.
    /// </summary>
    /// <param name="message">The message you want to send the user.</param>
    /// <param name="label1">The label you want the message to be rendered onto.</param>
    /// <param name="label2">Optionally used to clear the contents of another label on the page.</param>
    /// <param name="alert">If true, the message is displayed as a window alert. If false, the message is rendered onto the page.</param>

    private void SendMessage(string message, Label label1, Label label2 = null, bool alert = false) {
        if (alert == true) {
            Response.Write("<script language=javascript>alert('" + message + "');</script>");
            if (label1.Text != "") label1.Text = "";
        } else {
            label1.Text = message;
            if (label2 != null && label2.Text != "") label2.Text = "";
        }
    }

    /// <summary>
    /// Sets a given Panel's Visible property to true (if it isn't already true).
    /// </summary>
    /// <param name="numberOfPanels">The number of panel controls to be set to Visible.</param>
    /// <param name="partialPanelID">The panel ID, without any number designation. Used in conjuction with a counter variable to find the appropriate Panel control.</param>

    protected void ShowPanel(int numberOfPanels, string partialPanelID) {
        // Inspired by this StackOverflow question: https://stackoverflow.com/questions/19265103/find-div-tag-from-code-behind.

        for (int i = 1; i <= numberOfPanels; i++) {
            Panel panel = (Panel)FindControl(partialPanelID + i);
            if (panel.Visible != true) {
                panel.Visible = true;
            }
        }
    }

    /// <summary>
    /// Sets a given Panel's Visible property to false (if it isn't already false).
    /// </summary>
    /// <param name="maxPanels">The maximum number of panels which exist under a master control.</param>
    /// <param name="numberOfPanels">The number of panel controls to be set to Visible.</param>
    /// <param name="partialPanelID">The panel ID, without any number designation. Used in conjuction with a counter variable to find the appropriate Panel control.</param>
    /// <param name="partialTextboxID">The textbox ID, without any number designation. Used in conjuction with a counter variable to find the appropriate Textbox control.</param>
    /// <param name="partialTextbox2ID">Optionally used to find a second Textbox control.</param>

    protected void HidePanel(int maxPanels, int numberOfPanels, string partialPanelID, string partialTextboxID, string partialTextbox2ID = null) {
        // Works backwards from the maximum number of Panels, as to only hide panels that have not been requested.
        // e.g. When the user selects "4" from a dropdown list: hide Panel5, then Panel4.

        // Get the area of the room before resetting textbox values.
        double oldAreaOfRoom = CalculateRoomArea();

        for (int i = maxPanels; numberOfPanels < i; i--) {
            Panel panel = (Panel)FindControl(partialPanelID + i);
            if (panel.Visible == true) {
                panel.Visible = false;

                /// Clear textbox value(s).
                int counter = i;
                /// txtWallWidth1 is always visible, so start the counter at txtWallWidth2.
                if (partialTextboxID == "txtWallWidth")
                    counter += 1;
                TextBox textbox = (TextBox)FindControl(partialTextboxID + counter);
                EmptyTextbox(textbox);

                /// Clear textbox 2, if present.
                if (partialTextbox2ID != null) {
                    textbox = (TextBox)FindControl(partialTextbox2ID + counter);
                    EmptyTextbox(textbox);
                }
            }
        }

        // Get the area of the room after textboxes have been reset, and compare to the previous result.
        // If the values between the two variables differ, display the new result.

        double currentAreaOfRoom = CalculateRoomArea();
        if (lblPaintNeeded.Text != "" && currentAreaOfRoom != oldAreaOfRoom)
            DisplayResult();
    }

    /// <summary>
    /// Sets the appropriate child dropdownlist to visible, using the value of the parent control.
    /// </summary>
    /// <param name="paintBrand">The name of the paint brand.</param>
    /// <param name="paintBrandValue">The value of the parent control, indicating which paint brand was chosen.</param>

    public void DisplayDropdownlist(string paintBrand, int paintBrandValue) {
        if (paintBrand == "B&Q")
            paintBrand = "BQ";

        int previousBrandChosen;
        int.TryParse(Session["PaintBrandChosen"].ToString(), out previousBrandChosen); /// Returns 0 on the first call.

        if (previousBrandChosen != 0 && previousBrandChosen != paintBrandValue)
            // Reset the result, using the default coverage value of 12.
            DisplayResult();

        Session["PaintBrandChosen"] = paintBrandValue;

        if (paintBrandValue != 0) {
            pnlPaintMaster.Visible = true;
            string panelId = "pnl" + paintBrand;
            Panel panel = (Panel)FindControl(panelId);
            ResetSelectedIndex();
            SetPanelToVisible(panel);
        } else {
            pnlPaintMaster.Visible = false;
        }
    }

    /// <summary>
    /// Sets the selected index of all dropdownlists to -1, the default value.
    /// </summary>

    private void ResetSelectedIndex() {
        int defaultIndex = -1;

        ddlBQ.SelectedIndex = defaultIndex;
        ddlCrown.SelectedIndex = defaultIndex;
        ddlDulux.SelectedIndex = defaultIndex;
        ddlJohnstones.SelectedIndex = defaultIndex;
        ddlLeyland.SelectedIndex = defaultIndex;
        ddlValspar.SelectedIndex = defaultIndex;
        ddlWickes.SelectedIndex = defaultIndex;
    }

    /// <summary>
    /// Sets the appropriate paint brand panel to visible, while setting all others to false.
    /// </summary>
    /// <param name="panel">The panel to be set to visible.</param>

    private void SetPanelToVisible(Panel panel) {
        pnlBQ.Visible = false;
        pnlCrown.Visible = false;
        pnlDulux.Visible = false;
        pnlJohnstones.Visible = false;
        pnlLeyland.Visible = false;
        pnlValspar.Visible = false;
        pnlWickes.Visible = false;

        panel.Visible = true;
    }

    // Conversion

    /// <summary>
    /// Calculates the area of the room by converting the values of textbox and dropdownlist controls.
    /// </summary>
    /// <returns>Returns the area of the room.</returns>

    public double CalculateRoomArea() {
        double areaOfWalls = CalculateWallArea();
        double areaOfCeiling = CalculateCeilingArea();
        double areaOfWindows = CalculateWindowArea();
        double areaOfDoors = CalculateDoorArea();

        double result = areaOfWalls + areaOfCeiling - (areaOfDoors + areaOfWindows);
        return result;
    }

    private double CalculateDoorArea() {
        // All measurements are done in metres.
        const double doorHeight = 1.981;
        const double singleDoorWidth = 0.762;
        const double doubleDoorWidth = 1.524;

        int singleDoors = int.Parse(Session["SingleDoors"].ToString());
        int doubleDoors = int.Parse(Session["DoubleDoors"].ToString());

        double singleDoorArea = (doorHeight * singleDoorWidth) * singleDoors;
        double doubleDoorArea = (doorHeight * doubleDoorWidth) * doubleDoors;

        return singleDoorArea + doubleDoorArea;
    }

    private double CalculateWallArea() {        
        double usrWallHeight = Convert.ToDouble(txtWallHeight.Text);
        List<double> wallWidths = new List<double>;
        wallWidths[0] = Convert.ToDouble(txtWallWidth.Text);
        
        const int maxWalls = 8;
        Textbox textbox = new TextBox;

        for (int i = 2; i <= maxWalls; i++) {
            textbox.ID = "txtWallWidth" + i;
            wallWidths[i - 1] = Convert.ToDouble(textbox.Text);
        }

        double areaOfWalls = usrWallHeight * (wallWidths[0] + wallWidths[1] + wallWidths[2] +
         wallWidths[3] + wallWidths[4] + wallWidths[5] + wallWidths[6] + wallWidths[7]);
        
        return areaOfWalls;
    }

    private double CalculateCeilingArea() {
        double usrCeilingHeight;
        double.TryParse(txtCeilingHeight.Text, out usrCeilingHeight);
        double usrCeilingWidth;
        double.TryParse(txtCeilingWidth.Text, out usrCeilingWidth);

        double areaOfCeiling = (usrCeilingHeight * usrCeilingWidth);
        return areaOfCeiling;
    }

    private double CalculateWindowArea() {
        double usrWindowHeight;
        double.TryParse(txtWindowHeight1.Text, out usrWindowHeight);
        double usrWindowWidth;
        double.TryParse(txtWindowWidth1.Text, out usrWindowWidth);
        double usrWindowHeight2;
        double.TryParse(txtWindowHeight2.Text, out usrWindowHeight2);
        double usrWindowWidth2;
        double.TryParse(txtWindowWidth2.Text, out usrWindowWidth2);
        double usrWindowHeight3;
        double.TryParse(txtWindowHeight3.Text, out usrWindowHeight3);
        double usrWindowWidth3;
        double.TryParse(txtWindowWidth3.Text, out usrWindowWidth3);
        double usrWindowHeight4;
        double.TryParse(txtWindowHeight4.Text, out usrWindowHeight4);
        double usrWindowWidth4;
        double.TryParse(txtWindowWidth4.Text, out usrWindowWidth4);

        double areaOfWindows = (usrWindowHeight * usrWindowWidth) + (usrWindowHeight2 * usrWindowWidth2) + (usrWindowHeight3 * usrWindowWidth3) + (usrWindowHeight4 * usrWindowWidth4);
        return areaOfWindows;
    }

    /// <summary>
    /// Calculates the paint needed to decorate a room.
    /// </summary>
    /// <returns>Returns the volume of paint needed (in litres).</returns>
    /// <param name="areaOfRoom">Used to pass in the area of the room, preferably by method call.</param>
    /// <param name="coatsOfPaint">The number of coats of paint that will be used, preferably from a control.</param>
    /// <param name="coverageValue">Used to set the coverage value (in m² per litre).</param>

    public double CalculatePaintNeeded(double areaOfRoom, int coatsOfPaint, double coverageValue) {
        double totalPaintNeeded = areaOfRoom * coatsOfPaint / coverageValue;
        return totalPaintNeeded;
    }

    /// <summary>
    /// Retrieves the appropriate coverage value for the situation (e.g. if user hasn't selected a value, if a pre-determined value has been chosen).
    /// </summary>
    /// <param name="tableName">The name of the SQL table.</param>
    /// <param name="databaseID">The ID of the database object.</param>
    /// <param name="coverageTextbox">The textbox used to enter a custom coverage value.</param>
    /// <param name="coverageColumn">An optional parameter, used to pass the field name of the coverage column.</param>
    /// <returns>Returns the coverage value.</returns>

    private double GetCoverageValue(string tableName, int databaseID, TextBox coverageTextbox, string coverageColumn = "Coverage") {
        string coverageString = QueryDatabase("Coverage", tableName, databaseID);
        double coverageValue = double.Parse(coverageString);
        Session["CoverageValue"] = coverageValue;
        return coverageValue;
    }

    /// <summary>
    /// Queries the Paint database using a standard SELECT statement.
    /// </summary>
    /// <param name="column">The database column to read from.</param>
    /// <param name="table">The name of the database table.</param>
    /// <param name="databaseID">The identifier for the database object.</param>
    /// <param name="dbFileName">An optional string, used to pass in the name of the database.</param>
    /// <returns>The return data from the database is returned as a string.</returns>

    private string QueryDatabase(string column, string table, int databaseID, string dbFileName = "PaintDb.accdb") {
        // Source code: http://mdixon.soc.plym.ac.uk/_Content/2016-2017/Soft051_Lecture_13.ppt
        // Following code is adapted from the SOFT051 lecture slides.

        String connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;" +
              "Data Source=" + Server.MapPath(dbFileName) + ";";
        OleDbConnection connection = new OleDbConnection(connectionString);
        OleDbCommand command;
        OleDbDataReader reader;
        String result = "";

        command = new OleDbCommand("SELECT " + column + " FROM " + table + " WHERE ID = " + databaseID + " ;", connection);
        connection.Open();
        reader = command.ExecuteReader();
        while (reader.Read()) {
            result += reader[column];
        }
        connection.Close();

        return result;
    }

    // Collections

    public string GetPaintBrand(int brandId) {        
        // https://stackoverflow.com/questions/4438169/how-can-i-initialize-a-c-sharp-list-in-the-same-line-i-declare-it-ienumerable/4438190#4438190
        List<string> brands = new List<string> { "BQ", "Crown", "Dulux", "Johnstones",
        "Leyland", "Valspar", "Wickes" };

        return brands[brandId];
    }
}
