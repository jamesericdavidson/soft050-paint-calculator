﻿<!--
    Copyright 2017 James Davidson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
-->

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MainPage.aspx.cs" Inherits="MainPage" Async="true" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Interior Paint Calculator</title>
    <link runat="server" rel="stylesheet" href="~/stylesheet.css" type="text/css" media="screen">
    <script src="javascript.js" type="text/javascript"></script>
</head>
<body>
    <div id="container">
        <div id="header">
        </div>
        <div id="body">
            <form id="form1" runat="server">
                <h1>Interior Paint Calculator</h1>
                <h3>The easy way to find out how much paint you need.</h3>
                <hr />
                <br />
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                    <Scripts>
                        <asp:ScriptReference Path="~/RestoreFocus.js" />
                    </Scripts>
                </asp:ScriptManager>
                <asp:UpdatePanel runat="server" ID="MandatoryControlUpdatePanel" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlNumberOfWalls" />
                    </Triggers>
                    <ContentTemplate>
                        In the room, how many walls will you paint?:
                    <asp:DropDownList runat="server" ID="ddlNumberOfWalls" AutoPostBack="true" OnSelectedIndexChanged="ddlNumberOfWalls_SelectedIndexChanged">
                        <asp:ListItem Value="1" Text="1"></asp:ListItem>
                        <asp:ListItem Value="2" Text="2"></asp:ListItem>
                        <asp:ListItem Value="3" Text="3"></asp:ListItem>
                        <asp:ListItem Value="4" Text="4"></asp:ListItem>
                        <asp:ListItem Value="5" Text="5"></asp:ListItem>
                        <asp:ListItem Value="6" Text="6"></asp:ListItem>
                        <asp:ListItem Value="7" Text="7"></asp:ListItem>
                        <asp:ListItem Value="8" Text="8"></asp:ListItem>
                    </asp:DropDownList><br />
                        <br />
                        <asp:Label runat="server" ID="lblTip" CssClass="info" Text="Tip: Start measuring wall height from the top of the skirting board."></asp:Label><br />
                        <br />
                        <asp:Label runat="server" AssociatedControlID="txtWallHeight" Text="Wall Height (m): ">
                            <asp:TextBox runat="server" ID="txtWallHeight" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox>
                        </asp:Label>
                        <br />
                        <asp:Label runat="server" AssociatedControlID="txtWallWidth1" Text="Wall 1 Width (m): ">
                            <asp:TextBox runat="server" ID="txtWallWidth1" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                        </asp:Label><br />
                        <asp:Panel runat="server" ID="pnlWall1" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtWallWidth2" Text="Wall 2 Width (m): ">
                                <asp:TextBox runat="server" ID="txtWallWidth2" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlWall2" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtWallWidth3" Text="Wall 3 Width (m): ">
                                <asp:TextBox runat="server" ID="txtWallWidth3" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlWall3" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtWallWidth4" Text="Wall 4 Width (m): ">
                                <asp:TextBox runat="server" ID="txtWallWidth4" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlWall4" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtWallWidth5" Text="Wall 5 Width (m): ">
                                <asp:TextBox runat="server" ID="txtWallWidth5" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlWall5" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtWallWidth6" Text="Wall 6 Width (m): ">
                                <asp:TextBox runat="server" ID="txtWallWidth6" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlWall6" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtWallWidth7" Text="Wall 7 Width (m): ">
                                <asp:TextBox runat="server" ID="txtWallWidth7" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlWall7" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtWallWidth8" Text="Wall 8 Width (m): ">
                                <asp:TextBox runat="server" ID="txtWallWidth8" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <br />
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel runat="server" ID="OptionalControlUpdatePanel" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="cbCeiling" />
                        <%--<asp:AsyncPostBackTrigger ControlID="ddlNumberOfDoors" />
                        <asp:AsyncPostBackTrigger ControlID="cbCustomDoor" />--%>
                        <asp:AsyncPostBackTrigger ControlID="ddlNumberOfWindows" />
                        <asp:AsyncPostBackTrigger ControlID="ddlPaintBrand" />
                        <asp:AsyncPostBackTrigger ControlID="ddlBQ" />
                        <asp:AsyncPostBackTrigger ControlID="ddlDulux" />
                        <asp:AsyncPostBackTrigger ControlID="ddlCrown" />
                        <asp:AsyncPostBackTrigger ControlID="ddlJohnstones" />
                        <asp:AsyncPostBackTrigger ControlID="ddlLeyland" />
                        <asp:AsyncPostBackTrigger ControlID="ddlValspar" />
                        <asp:AsyncPostBackTrigger ControlID="ddlWickes" />
                        <asp:AsyncPostBackTrigger ControlID="btnClear" />
                    </Triggers>
                    <ContentTemplate>
                        Are you going to paint the ceiling (with the same paint?)
            <asp:CheckBox ID="cbCeiling" runat="server" AutoPostBack="true" OnCheckedChanged="cbCeiling_CheckedChanged" /><br />
                        <asp:Panel runat="server" ID="pnlCeiling" Visible="false">
                            <br />
                            <asp:Label runat="server" AssociatedControlID="txtCeilingHeight" Text="Ceiling Height (m): ">
                                <asp:TextBox runat="server" ID="txtCeilingHeight" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                            <asp:Label runat="server" AssociatedControlID="txtCeilingWidth" Text="Ceiling Width (m): ">
                                <asp:TextBox runat="server" ID="txtCeilingWidth" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <br />
                        How many coats of paint will you use?:
            <asp:DropDownList runat="server" ID="ddlCoatsOfPaint" OnSelectedIndexChanged="UpdateResult" AutoPostBack="true">
                <asp:ListItem Selected="True" Value="1" Text="1"></asp:ListItem>
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
            </asp:DropDownList>
                        <br />
                        <br />
                        <hr />
                        <br />
                        <%--How many doors are there in the room?:
            <asp:DropDownList runat="server" ID="ddlNumberOfDoors" AutoPostBack="true" OnSelectedIndexChanged="ddlNumberOfDoors_SelectedIndexChanged">
                <asp:ListItem Value="0" Text="No door"></asp:ListItem>
                <asp:ListItem Selected="True" Value="1" Text="Single door"></asp:ListItem>
                <asp:ListItem Value="2" Text="Two doors"></asp:ListItem>
                <asp:ListItem Value="3" Text="Three doors"></asp:ListItem>
            </asp:DropDownList><br />
                        <br />
                        <asp:Panel runat="server" ID="pnlToggleCustomDoor" Visible="true">
                            <i>Using the standard 2'6" internal door size (1981 x 762 mm). Add your own measurements?</i>
                            <asp:CheckBox runat="server" ID="cbCustomDoor" OnCheckedChanged="cbCustomDoor_CheckedChanged" AutoPostBack="true" /><br />
                            <br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlCustomDoor" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtDoorHeight" Text="Door Height (mm): ">
                                <asp:TextBox runat="server" ID="txtDoorHeight" Text="1981" OnTextChanged="SendDoorMeasurements" AutoPostBack="true"></asp:TextBox>
                            </asp:Label><br />
                            <asp:Label runat="server" AssociatedControlID="txtDoorWidth" Text="Door Width (mm): ">
                                <asp:TextBox runat="server" ID="txtDoorWidth" OnTextChanged="SendDoorMeasurements" AutoPostBack="true"></asp:TextBox>
                            </asp:Label><br />
                            <br />
                        </asp:Panel>--%>
                        <%-- Single & double door textboxes --%>
                        How many doors are there in the room?:
                        <br /><br />
                        <asp:Label runat="server" AssociatedControlID="txtSingleDoors" Text="Single doors: ">
                            <asp:TextBox runat="server" ID="txtSingleDoors" MaxLength="1" OnTextChanged="SendDoorNumbers" AutoPostBack="true"></asp:TextBox>
                        </asp:Label><br />
                        <asp:Label runat="server" AssociatedControlID="txtDoubleDoors" Text="Double doors: ">
                            <%-- B&Q take double doors to be double the size of a common single door
                                Making that a 1524 x 1981mm door --%>
                            <asp:TextBox runat="server" ID="txtDoubleDoors" MaxLength="1" OnTextChanged="SendDoorNumbers" AutoPostBack="true"></asp:TextBox>
                        </asp:Label><br />
                        <br />
                        And how many windows?:
            <asp:DropDownList runat="server" ID="ddlNumberOfWindows" AutoPostBack="true" OnSelectedIndexChanged="ddlNumberOfWindows_SelectedIndexChanged">
                <asp:ListItem Selected="True" Value="0" Text="No windows"></asp:ListItem>
                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                <asp:ListItem Value="4" Text="4"></asp:ListItem>
            </asp:DropDownList><br />
                        <asp:Panel runat="server" ID="pnlWindow1" Visible="false">
                            <br />
                            <asp:Label runat="server" AssociatedControlID="txtWindowHeight1" Text="Window Height (m): ">
                                <asp:TextBox runat="server" ID="txtWindowHeight1" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                            <asp:Label runat="server" AssociatedControlID="txtWindowWidth1" Text="Window Width (m): ">
                                <asp:TextBox runat="server" ID="txtWindowWidth1" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlWindow2" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtWindowHeight2" Text="Window 2 Height (m): ">
                                <asp:TextBox runat="server" ID="txtWindowHeight2" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                            <asp:Label runat="server" AssociatedControlID="txtWindowWidth2" Text="Window 2 Width (m): ">
                                <asp:TextBox runat="server" ID="txtWindowWidth2" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlWindow3" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtWindowHeight3" Text="Window 3 Height (m): ">
                                <asp:TextBox runat="server" ID="txtWindowHeight3" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                            <asp:Label runat="server" AssociatedControlID="txtWindowWidth3" Text="Window 3 Width (m): ">
                                <asp:TextBox runat="server" ID="txtWindowWidth3" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlWindow4" Visible="false">
                            <asp:Label runat="server" AssociatedControlID="txtWindowHeight4" Text="Window 4 Height (m): ">
                                <asp:TextBox runat="server" ID="txtWindowHeight4" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                            <asp:Label runat="server" AssociatedControlID="txtWindowWidth4" Text="Window 4 Width (m): ">
                                <asp:TextBox runat="server" ID="txtWindowWidth4" OnTextChanged="UpdateResult" AutoPostBack="true"></asp:TextBox><br />
                            </asp:Label><br />
                        </asp:Panel>
                        <br />
                        <hr />
                        <br />
                        <%--<p class="info">If you know which paint you're going to use, select it to get a more accurate result.</p>--%>
                        <asp:Label runat="server" ID="lblTip2" CssClass="info" Text="If you know which paint you're going to use, select it to get a more accurate result."></asp:Label>
                        <br />
                        <br />
                        Choose the paint brand:
            <asp:DropDownList runat="server" ID="ddlPaintBrand" OnSelectedIndexChanged="ddlPaintBrand_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Value="0" Selected="True" Text=""></asp:ListItem>
                <asp:ListItem Value="1" Text="BQ"></asp:ListItem>
                <%-- Change name --%>
                <asp:ListItem Value="2" Text="Crown"></asp:ListItem>
                <asp:ListItem Value="3" Text="Dulux"></asp:ListItem>
                <asp:ListItem Value="4" Text="Johnstones"></asp:ListItem>
                <asp:ListItem Value="5" Text="Leyland"></asp:ListItem>
                <asp:ListItem Value="6" Text="Valspar"></asp:ListItem>
                <asp:ListItem Value="7" Text="Wickes"></asp:ListItem>
            </asp:DropDownList><br />
                        <br />
                        <asp:Panel runat="server" ID="pnlPaintMaster" Visible="false">
                            <asp:Panel runat="server" ID="pnlDulux" Visible="false">
                                <asp:AccessDataSource runat="server" ID="adsDulux" DataSourceMode="DataReader" DataFile="~/PaintDb.accdb" SelectCommand="SELECT ID,Paint FROM Dulux"></asp:AccessDataSource>
                                Select your product:
                    <asp:DropDownList runat="server" ID="ddlDulux" OnSelectedIndexChanged="ddlDulux_SelectedIndexChanged" AutoPostBack="true"
                        DataSourceID="adsDulux" DataTextField="Paint" DataValueField="ID">
                    </asp:DropDownList>
                                <br />
                                <br />
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlWickes" Visible="false">
                                <asp:AccessDataSource runat="server" ID="adsWickes" DataSourceMode="DataReader" DataFile="~/PaintDb.accdb" SelectCommand="SELECT ID,Paint FROM Wickes"></asp:AccessDataSource>
                                Select your product:
                    <asp:DropDownList runat="server" ID="ddlWickes" OnSelectedIndexChanged="ddlWickes_SelectedIndexChanged" AutoPostBack="true"
                        DataSourceID="adsWickes" DataTextField="Paint" DataValueField="ID">
                    </asp:DropDownList>
                                <br />
                                <br />
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlCrown" Visible="false">
                                <asp:AccessDataSource runat="server" ID="adsCrown" DataSourceMode="DataReader" DataFile="~/PaintDb.accdb" SelectCommand="SELECT ID,Paint FROM Crown"></asp:AccessDataSource>
                                Select your product:
                    <asp:DropDownList runat="server" ID="ddlCrown" OnSelectedIndexChanged="ddlCrown_SelectedIndexChanged" AutoPostBack="true"
                        DataSourceID="adsCrown" DataTextField="Paint" DataValueField="ID">
                    </asp:DropDownList>
                                <br />
                                <br />
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlJohnstones" Visible="false">
                                <asp:AccessDataSource runat="server" ID="adsJohnstones" DataSourceMode="DataReader" DataFile="~/PaintDb.accdb" SelectCommand="SELECT ID,Paint FROM Johnstones"></asp:AccessDataSource>
                                Select your product:
                    <asp:DropDownList runat="server" ID="ddlJohnstones" OnSelectedIndexChanged="ddlJohnstones_SelectedIndexChanged" AutoPostBack="true"
                        DataSourceID="adsJohnstones" DataTextField="Paint" DataValueField="ID">
                    </asp:DropDownList>
                                <br />
                                <br />
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlBQ" Visible="false">
                                <asp:AccessDataSource runat="server" ID="adsBQ" DataSourceMode="DataReader" DataFile="~/PaintDb.accdb" SelectCommand="SELECT ID,Paint FROM BQ"></asp:AccessDataSource>
                                Select your product:
                    <asp:DropDownList runat="server" ID="ddlBQ" OnSelectedIndexChanged="ddlBQ_SelectedIndexChanged" AutoPostBack="true"
                        DataSourceID="adsBQ" DataTextField="Paint" DataValueField="ID">
                    </asp:DropDownList>
                                <br />
                                <br />
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlLeyland" Visible="false">
                                <asp:AccessDataSource runat="server" ID="adsLeyland" DataSourceMode="DataReader" DataFile="~/PaintDb.accdb" SelectCommand="SELECT ID,Paint FROM Leyland"></asp:AccessDataSource>
                                Select your product:
                    <asp:DropDownList runat="server" ID="ddlLeyland" OnSelectedIndexChanged="ddlLeyland_SelectedIndexChanged" AutoPostBack="true"
                        DataSourceID="adsLeyland" DataTextField="Paint" DataValueField="ID">
                    </asp:DropDownList>
                                <br />
                                <br />
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlValspar" Visible="false">
                                <asp:AccessDataSource runat="server" ID="adsValspar" DataSourceMode="DataReader" DataFile="~/PaintDb.accdb" SelectCommand="SELECT ID,Paint FROM Valspar"></asp:AccessDataSource>
                                Select your product:
                    <asp:DropDownList runat="server" ID="ddlValspar" OnSelectedIndexChanged="ddlValspar_SelectedIndexChanged" AutoPostBack="true"
                        DataSourceID="adsValspar" DataTextField="Paint" DataValueField="ID">
                    </asp:DropDownList>
                                <br />
                                <br />
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlCustomCoverage" Visible="true">
                            Already found the paint you need? Type in the coverage value here: 
                <asp:TextBox runat="server" ID="txtCustomCoverage" OnTextChanged="txtCustomCoverage_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" />
                            <br />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <%-- Result panel --%>
                <asp:UpdatePanel runat="server" ID="ResultUpdatePanel" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlResult" CssClass="result" Visible="false">
                            <asp:Label runat="server" ID="lblPaintNeeded"></asp:Label><br />
                            <br />
                            <asp:Label runat="server" ID="lblRoomArea"></asp:Label>
                        </asp:Panel>
                        <%-- Break for print --%>
                        <br />
                        <asp:Panel runat="server" ID="pnlCoverage" CssClass="result2" Visible="false">
                            <asp:Label runat="server" ID="lblCoverageValue"></asp:Label>
                            <asp:HyperLink runat="server" ID="hlDefaultCoverage" ImageUrl="~/question.png" Visible="false" NavigateUrl="http://www.coatings.org.uk/faq/How_much_paint_to_use-7.aspx"
                                ToolTip="As defined by the British Coatings Federation." Target="_blank" CssClass="hyperlink"></asp:HyperLink>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <br />
                <div id="pagefunctions">
                    <input type="button" id="btnReset" value="Reset" class="reset-button" onclick="refreshPage()" />
                    <input type="button" id="btnPrint" value="Print" class="print-button" onclick="printResult()" />
                </div>
            </form>
        </div>
        <div id="footer">
        </div>
    </div>
</body>
</html>
