﻿/*
 * Copyright 2017 James Davidson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Source code from Madhu: https://stackoverflow.com/questions/9732455/how-to-allow-only-integers-in-a-textbox/9732876
function isNumberKey(evt) {
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

// Source code from Sajeel: https://stackoverflow.com/questions/22405246/how-to-allow-only-digits-and-one-decimal-point-in-a-web-form-textbox
function isDecimal(evt) {
    var charCode = evt.which ? evt.which : event.keyCode;
    var parts = evt.srcElement.value.split('.');
    if (parts.length > 1 && charCode === 46)
        return false;
    else {
        if (charCode === 46 || charCode >= 48 && charCode <= 57)
            return true;
        return false;
    }
}

// Inspired by: https://www.w3schools.com/jsref/met_win_confirm.asp
function refreshPage() {
    var refreshRequest = confirm("This will wipe any values you've already entered! Are you sure you want to reset the calculator?");
    if (refreshRequest === true)
        location.reload();
    else return;
}

// Inspired by: https://www.aspsnippets.com/Articles/Print-specific-part-of-web-page-in-ASPNet.aspx
function printResult() {
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title>Print your results</title>');
    printWindow.document.write('<link rel="stylesheet" type="text/css" href="PrintWindow.css">');
    printWindow.document.write('</head><body>');
    printWindow.document.write('<h1>Paint Calculator Results</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write(ResultUpdatePanel.innerHTML);
    printWindow.document.write('</body></html>');
    printWindow.document.close();

    setTimeout(function () {
        printWindow.print();
    }, 200);
    return false;
}
