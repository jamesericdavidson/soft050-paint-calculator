# Paint Calculator

Coursework for SOFT050 - Computing Project

*Estimate how many litres of paint are needed to paint a room*

---

- Documentation is required for this project, as it encompasses multiple technologies:
    - ASP.NET (C#)
    - JavaScript
    - Access Database
    - HTML
    - CSS

- The code requires a major refactor for readability

```
Copyright 2017 James Davidson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
